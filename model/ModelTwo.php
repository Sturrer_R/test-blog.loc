<?php

require_once "ModelOne.php";

class ModelTwo extends ModelOne
{
    public function insertComment($name, $comment, $id)
    {
        $name = $this->clean($name);
        $comment = $this->clean($comment);
        $sql = $this->db->prepare("INSERT INTO comments (username,comment,post_id)  VALUES (:name,:text,:id)");
        $sql->bindParam(':name', $name);
        $sql->bindParam(':text', $comment);
        $sql->bindParam(':id', $id);
        $sql->execute();
    }

    public function selectPost($id)
    {
        $query = $this->db->prepare("SELECT * FROM  posts WHERE id = :post_id ");
        $query->bindParam(':post_id', $id);
        $query->execute();
        $result = $query->fetch();

        if ($result['id'] == '') {
            header('Location: /');
            exit;
        }
    }

    public function selectComments()
    {
        $q = "SELECT * FROM comments join  posts on post_id = posts.id";
        $query = $this->db->prepare($q);
        $query->execute();
        $res = $query->fetchAll();
        return $res;
    }

    public function countComments($id)
    {
        $count = $this->db->prepare("SELECT COUNT(*) FROM comments where post_id = :id");
        $count->bindParam(':id', $id);
        $count->execute();
        $num = $count->fetchColumn();
        return $num;
    }
}
<?php

class ModelOne
{
    protected $db;
    protected $host = "localhost";
    protected $db_name = "db_blog";
    protected $charset = "utf8";
    protected $username = "root";
    protected $passw = "root";


    public function __construct()
    {
        try {
            $this->db = new PDO("mysql:host=$this->host;dbname=$this->db_name;charset=$this->charset",
                $this->username, $this->passw);
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function clean($value = "")
    {
        $value = trim($value);
        $value = stripslashes($value);
        $value = strip_tags($value);
        $value = htmlspecialchars($value);

        return $value;
    }

    public function insert($name, $post)
    {
            $name = $this->clean($name);
            $post = $this->clean($post);

            $query = $this->db->prepare("INSERT INTO posts (name,message) VALUES (:username, :text)");
            $query->bindParam(':username', $name);
            $query->bindParam(':text', $post);
            $query->execute();

    }

    public function renderPostsPopular()
    {
            $popular = $this->db->prepare("SELECT posts.id,posts.name,posts.message,posts.date, 
                (SELECT COUNT(*) FROM comments WHERE post_id = posts.id) as comm_n 
                FROM posts ORDER BY comm_n DESC LIMIT 5 ");
            $popular->execute();
            $pop_res = $popular->fetchAll();
            return $pop_res;

    }

    public function countPosts()
    {
            $pagin = $this->db->prepare("SELECT COUNT(*) FROM posts");
            $pagin->execute();
            $res = $pagin->fetch();
            return $res;
    }

    public function paginatePosts($offset, $limit)
    {
            $sql = "SELECT posts.id ,posts.name,posts.message,posts.date,
            (SELECT COUNT(*) FROM comments WHERE post_id = posts.id)
            as comm_number FROM posts ORDER BY posts.date DESC LIMIT $offset, $limit";

            $query = $this->db->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();
            return $result;
    }



}
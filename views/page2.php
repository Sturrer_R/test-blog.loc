<?php
require_once "../model/ModelTwo.php";

$usrname = $_POST['username'];
$usrcomment = $_POST['comment'];

try {
    $model = new ModelTwo();
    $id = $_GET['id'];
    if (isset($_POST['submit2'])) {
        $model->insertComment($usrname, $usrcomment, $id);
    }

    //select data of specific post
    $result = $model->selectPost($id);

    //select comments for specific message
    $res_val = $model->selectComments();

    //count all comments
    $num_comments = $model->countComments($id);

} catch (PDOException $e) {
    echo $e->getMessage();
}
?>


<html lang="en">

<head>

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Blog Post</title>

    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/blog-post.css" rel="stylesheet">
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css'
          integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ'
          crossorigin='anonymous'>
</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="#">Simple Blog</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="/">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>

            </ul>
        </div>
    </div>
</nav>

<!-- Page Content -->
<div class="container">

    <div class="row">
        <div class="col-lg-8">

            <h1 class="mt-4">Post</h1>

            <p class="lead" style="word-wrap: break-word"><?= $result['message'] ?></p>
            <hr>
            <p>Posted on January 1, 2019 at 12:00 PM by
                <a href="#"><?= $result['name'] ?></a></p>
            <i class="fa fa-comments" aria-hidden="true" style="font-size:20px;"> </i>
            <?= $num_comments ?>
            <hr>

            <!-- Comments Form -->
            <div class="card my-4">
                <h5 class="card-header">Leave a Comment:</h5>
                <div class="card-body">
                    <form name="form2" method="post">
                        <div class="form-group">
                            <input type="text" class="form-control" name="username" placeholder="Enter your name"
                                   required>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" rows="3" name="comment" placeholder="Enter your comment"
                                      required></textarea>
                        </div>
                        <button type="submit" class="btn btn-success" name="submit2">Send</button>
                    </form>
                </div>
            </div>

            <!-- Single Comment -->

            <?php foreach ($res_val as $v): ?>
                <?php if ($id == $v['post_id']): ?>
                    <div class="media mb-4">

                        <div class="media-body">
                            <h5 class="mt-0"><a href="#"><?= $v['username'] ?></a></h5>
                            <?= $v['comment'] ?>
                            <p>Created <?= $v['date'] ?></p>
                        </div>
                    </div>
                <? endif; ?>
            <?php endforeach; ?>

        </div>


        <!-- /.row -->

    </div>
    <!-- /.container -->
</div>


</body>

</html>

<?php
require_once "model/ModelOne.php";

$username = $_POST['user_name'];
$usertext = $_POST['user_message'];

try {
    $db = new ModelOne();
    if (isset($_POST['submit1'])) {
        $db->insert($username, $usertext);
    }

    $page = isset($_GET["page"]) ? (int)$_GET["page"] : 1;//get current page

    $limit = 3; //number of posts for render
    $shift = ($page * $limit) - $limit; //get the number of post to start rendering

    //count all posts
    $count = $db->countPosts();

    $total_posts = $count[0]; //all posts in the table

    $str_pag = ceil($total_posts / $limit); //get the number of pages
    $result = $db->paginatePosts($shift, $limit); // paginate last 5 posts

    //render popular posts for slider
    $popular_posts = $db->renderPostsPopular();

} catch (PDOException $e) {
    echo $e->getMessage();
}

?>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/blog-home.css" rel="stylesheet">
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css'
          integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ'
          crossorigin='anonymous'>
    <script src="../js/jquery-min.js"></script>
    <title>Simple Blog</title>

</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="#">Simple Blog</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

    </div>
</nav>

<div class="container">

    <div class="slider">
        <h2 class="mt-2" style="text-align: center; margin: -33px auto;">Popular posts</h2>
    <?php foreach ($popular_posts as $val):?>
    <div class="card mb-4 myslides">
        <div class="card-body">
            <p class="card-text"><?= substr($val['message'], 0, 100) ?></p>
            <a href="views/page2.php?id=<?= $val['id'] ?>" class="btn btn-primary">Read More &rarr;</a>
        </div>
        <div class="card-footer text-muted">
            <?= $val['date'] ?>
            <a href="#">by <?= $val['name'] ?></a>

            <i class="fa fa-comments" aria-hidden="true" style="font-size:20px;"> </i>
            <?= $val['comm_n'] ?>
        </div>
    </div>
    <?php endforeach;?>
    </div>


    <div class="row">

        <div class="col-md-6" style="margin-top: 10%;">

            <!-- Slider -->
            <?php foreach ($result as $res): ?>

                <div class="card mb-4">
                    <div class="card-body">
                        <p class="card-text"><?= substr($res['message'], 0, 100) ?></p>
                        <a href="views/page2.php?id=<?= $res['id'] ?>" class="btn btn-primary">Read More &rarr;</a>
                    </div>
                    <div class="card-footer text-muted">
                        <?= $res['date'] ?>
                        <a href="#">by <?= $res['name'] ?></a>

                        <i class="fa fa-comments" aria-hidden="true" style="font-size:20px;"> </i>
                        <?= $res['comm_number'] ?>
                    </div>
                </div>
            <?php endforeach; ?>

            <!-- Pagination -->
            <ul class="pagination">
                <li class="page-item <?php if ($page <=1) echo 'disabled';?>  ">
                    <a class="page-link" href="?page=<?=($page - 1)?>" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">Previous</span>
                    </a>
                </li>
                <?php for ($i = 1; $i <= $str_pag; $i++):?>
                <li class="page-item">
                    <a class="page-link " href="?page=<?=$i?>"> <?=$i?> </a>
                </li>
                <?php endfor;?>

                <li class="page-item <?php if ($page>=$str_pag) echo 'disabled';?>">
                    <a class="page-link" href="?page=<?=($page + 1)?>" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                    </a>
                </li>
            </ul>

        </div>

        <!-- Posts Creation Form -->
        <div class="col-md-6" style="margin-top: 8%;">

            <div class="card my-4" >
                <h5 class="card-header">Create new post</h5>
                <div class="card-body">
                    <form name="form1" method="post">
                        <div class="form-group">
                            <input type="text" class="form-control" name="user_name" placeholder="Enter your name" required>
                        </div>
                        <div class="form-group">
                            <textarea name="user_message" class="form-control" placeholder="Enter your text"
                                      rows="5" required></textarea>
                        </div>
                        <button type="submit" class="btn btn-success" name="submit1">Send</button>
                    </form>
                </div
            </div>

        </div>

    </div>
    <!-- /.row -->

</div>

<!-- /.container -->


</body>

</html>

<script>

    $(document).ready(function () {

        var slideIndex = 0;
        carousel();

        function carousel() {
            var i;
            var x = $(".mySlides");
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";
            }
            slideIndex++;
            if (slideIndex > x.length) slideIndex = 1;
            x[slideIndex - 1].style.display = "block";
            setTimeout(carousel, 4000);
        }

    });
</script>

